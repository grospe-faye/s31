// contain all business logic

const Task = require('../models/taskSchema')

// Get All Tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

// Creating New Task
module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then((task, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return task
		}
	})
}

// Delete Task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndDelete(taskId).then((removedTask, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}

// Update task 
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		}
		result.name = newContent.name
			return result.save().then((updatedTask, saveErr) => {
				if(saveErr){
					console.log(saveErr)
					return false
				} else {
					return updatedTask
				}
			})
	})
}

/*ACTIVITY*/
// Get specific task
module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result
	})
}

// Change status to complete
module.exports.changeStatusComplete = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		}
		result.status = newStatus.status
			return result.save().then((changeStatusComplete, saveErr) => {
				if(saveErr){
					console.log(saveErr)
					return false
				} else {
					return changeStatusComplete
				}
			})
	})
}